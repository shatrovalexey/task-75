<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Api extends Controller {
	public function action_content( ) {
		$data = file_get_Contents( 'https://meduza.io/api/w5/screens/news' ) ;
		$data = gzdecode( $data ) ;
		$data = json_decode( $data , true ) ;

		$keys = array_keys( $data[ 'documents' ] ) ;
		$keys = array_filter( $keys , function( $item ) {
		$result = mb_strpos( $item , 'news/' ) ;
			return ( $result !== false ) && ! $result ;
		} ) ;
		$document = $data[ 'documents' ][ $keys[ array_rand( $keys ) ] ] ;
		$result = json_encode( $document ) ;

		$this->response->body( $result ) ;
	}
}